require('dotenv').config();
const port = process.env.PORT || 3000;
const express = require('express');
const app = express();
const morgan = require('morgan');
const expressBearerToken = require('express-bearer-token');
const packets = require('./src/packets');
const accounts = require('./src/accounts');
const playlists = require('./src/playlists');
const songs = require('./src/songs');
const helmet = require('helmet');

app.use(helmet());
app.use(morgan('combined'));
app.use(express.json());
app.use(expressBearerToken());
app.use(function (_, res, next) {
	res.sendServerError = packets.response.sendServerError;
	res.sendInvalidRequest = packets.response.sendInvalidRequest;
	res.sendUnauthenticatedRequest = packets.response.sendUnauthenticatedRequest;
	next();
});
app.get('/', function(_, res) {
	res.status(200);
	res.json(packets.factory.createResponse("Welcome to the Playlist Manager API.", packets.types.success, {}));
});
app.use('/accounts', accounts);
app.use('/playlists', playlists);
app.use('/songs', songs);
app.use('*', function(_, res) {
	console.error("Playlist Manager API -> Endpoint not found.");
	res.status(404);
	res.json(packets.factory.createResponse("Oh oh, page not found...", packets.types.error, {}));
});
app.listen(port, () => console.log(`Server listening on port ${port}!`));