const generateToken = require('../tokens/generate').generate;
const database = require('../database');
const packets = require('../packets');
const errors = require('../errors');
const bcrypt = require('bcrypt');

function authenticate(req, res) {
	let data = {
		email: req.body.email,
		password: req.body.password
	};

	if (!packets.verify.checkData(data) || !packets.verify.checkEmail(data.email)) {
		return res.sendInvalidRequest(res);
	}
	database.findOneInCollection("accounts", ["id", "fullname", "email", "password"], { "email": data.email })
	.then(function(account) {
		if (!account) {
			throw new errors.Request.Unauthenticated("Invalid credentials.");
		} else if (bcrypt.compareSync(data.password, account.password) == false) {
			throw new errors.Request.Unauthenticated("Invalid credentials.");
		} else {
			delete account.password;
			let token = generateToken(req._remoteAddress, account);
			res.status(200);
			res.json(packets.factory.createResponse("Logged in.", packets.types.success, {token: token, account: account}));
		}
	})
	.catch(function(err) {
		errors.intercept(res, err);
	});
}

module.exports = authenticate