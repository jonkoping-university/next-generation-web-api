const express = require('express');
const router = express.Router();
const verify = require('../tokens/verify').verify;
const create = require('./create');
const authenticate = require('./authenticate');
const get = require('./get');

router.post('/', function(req, res) {
	create.account(req, res);
});

router.post('/authenticate', function(req, res) {
	authenticate(req, res);
});

router.use(function(req, res, next) {
	verify(req, res, next);
});

router.get('/', function(req, res) {
	get.accounts(req, res);
});

router.get('/:id', function(req, res) {
	get.account(req, res);
});

module.exports = router;