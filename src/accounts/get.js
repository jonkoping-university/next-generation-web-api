const database = require('../database');
const packets = require('../packets');
const errors = require('../errors');

function getAccount(req, res) {
	let data = {
		id: Number(req.params.id)
	}

	if (!packets.verify.checkData(data)) {
		return res.sendInvalidRequest(res);
	}
	database.findOneInCollection("accounts", ["id", "fullname"], { id: data.id })
	.then(function(account) {
		if (!account) {
			throw new errors.Request.Unauthenticated();
		} else {
			res.status(200);
			return res.json(packets.factory.createResponse("Retrieved account.", packets.types.success, {account: account}));
		}
	}).catch(function(err) {
		errors.intercept(res, err);
	});
}

function getAccounts(req, res) {
	if (req.query.query) {
		database.searchAllAccounts(req)
		.then(function(accounts) {
			if (!accounts) {
				throw new errors.Request.Unauthenticated();
			} else {
				res.status(200);
				res.json(packets.factory.createResponse("Accounts retrieved.", packets.types.success, {accounts: accounts}))
			}
		})
		.catch(function(err) {
			errors.intercept(res, err);
		});
	} else {
		database.findAllAccounts(req)
		.then(function(accounts) {
			if (!accounts) {
				throw new errors.Request.Unauthenticated();
			} else {
				res.status(200);
				res.json(packets.factory.createResponse("Accounts retrieved.", packets.types.success, {accounts: accounts}))
			}
		})
		.catch(function(err) {
			errors.intercept(res, err);
		});
	}
}

module.exports = {
	account: getAccount,
	accounts: getAccounts
}