const database = require('../database');
const packets = require('../packets');
const errors = require('../errors');
const bcrypt = require('bcrypt');
const saltRounds = 10;

function createAccount(req, res) {
	let data = {
		fullname: req.body.fullname,
		email: req.body.email,
		password: req.body.password
	};

	if (!packets.verify.checkData(data) || !packets.verify.checkEmail(data.email)) {
		return res.sendInvalidRequest(res, "Missing informations or invalid email.");
	}
	bcrypt.hash(data.password, saltRounds, function (err, hash) {
		if (err) {
			return res.sendServerError(err, res);
		}
		data.password = hash;
		database.createAccount(data.fullname, data.email, data.password)
		.then(function() {
			res.status(200);
			res.json(packets.factory.createResponse("Registration was successfull.", packets.types.success, {}));
		})
		.catch(function(err) {
			if (err instanceof errors.Server.Database) {
				err = new errors.Request.Invalid("Email already taken.");
			}
			errors.intercept(res, err);
		});
	});
}

module.exports = {
	account: createAccount
}