const Custom = require('./custom').Custom;

class Server extends Custom {
	constructor (message, code) {
		super(message, code);
	}
}

class Database extends Server {
	constructor (message) {
		super(message, 500);
	}
}

module.exports = {
	Default: Server,
	Database: Database
};