const Server = require('./server');
const Request = require('./request');
const Custom = require('./custom').Custom;
const packets = require('../packets');

function intercept(res, err) {
	if (err instanceof Custom) {
		res.status(err.code);
	} else {
		res.status(500);
	}
	console.log(err);
	res.json(packets.factory.createResponse(err.message, packets.types.error, {}));
}

module.exports = {
	Custom: Custom,
	Server: Server,
	Request: Request,
	intercept: intercept
};