const Custom = require('./custom').Custom;

class RequestError extends Custom {
	constructor (message, code) {
		super(message, code);
	}
}

class UnauthenticatedError extends RequestError {
	constructor (message = "Unauthenticated request.") {
		super(message, 401);
	}
}

class UnauthorizedError extends RequestError {
	constructor (message = "Access denied.") {
		super(message, 401);
	}
}

class InvalidError extends RequestError {
	constructor (message = "Invalid request.") {
		super(message, 400);
	}
}

module.exports = {
	Default: RequestError,
	Unauthenticated: UnauthenticatedError,
	Unauthorized: UnauthorizedError,
	Invalid: InvalidError
}