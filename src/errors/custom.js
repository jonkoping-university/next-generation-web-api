class Custom extends Error {
	constructor (message, code) {
		super(message);
		this.code = code;
	}
}

module.exports = {
	Custom: Custom
}