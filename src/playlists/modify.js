const database = require('../database');
const packets = require('../packets');
const errors = require('../errors');

function modifyPlaylist(req, res) {
	let data = {
		playlist: Number(req.params.id),
		private: req.body.private,
		name: req.body.name,
		id: req.payload.id
	}

	if (!packets.verify.checkData(data)) {
		return res.sendInvalidRequest(res);
	}
	database.updatePlaylist(data.playlist, data.id, data.name, data.private)
	.then(function(changes) {
		if (changes == 0) {
			throw new errors.Request.Invalid("No matching playlist found.");
		}
		res.status(200);
		res.json(packets.factory.createResponse("Playlist updated.", packets.types.success, { playlist: data.playlist }));
	}).catch(function(err) {
		errors.intercept(res, err);
	});
}

module.exports = {
	modifyPlaylist
}