const database = require('../database');
const packets = require('../packets');
const errors = require('../errors');

function createPlaylist(req, res) {
	let data = {
		name: req.body.name,
		id: req.payload.id,
		modified: Date.now() / 1000
	};

	if (!packets.verify.checkData(data)) {
		return packets.response.sendInvalidRequest(res, "Missing informations.");
	}
	database.createPlaylist(data.name, data.id, true, data.modified)
	.then(function(playlist) {
		res.status(200);
		res.json(packets.factory.createResponse("Playlist created.", packets.types.success, {id: playlist}));
	})
	.catch(function(err) {
		errors.intercept(res, err);
	});
}

module.exports = {
	createPlaylist
}