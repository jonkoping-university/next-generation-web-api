const express = require('express');
const router = express.Router();
const verify = require('../tokens/verify').verify;
const create = require('./create').createPlaylist;
const get = require('./get');
const modify = require('./modify').modifyPlaylist;
const deletePlaylist = require('./delete').deletePlaylist;

router.use(function(req, res, next) {
	verify(req, res, next);
});
router.get('/', function(req, res) {
	return get.playlists(req, res);
});
router.post('/', function(req, res) {
	return create(req, res);
});
router.get('/:id', function(req, res) {
	return get.playlist(req, res);
});
router.put('/:id', function(req, res) {
	return modify(req, res);
});
router.delete('/:id', function(req, res) {
	return deletePlaylist(req, res);
});

module.exports = router;