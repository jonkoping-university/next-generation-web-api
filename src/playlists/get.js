const database = require('../database');
const packets = require('../packets');
const errors = require('../errors');
const search = require('./search').playlists;
const sparql = require('../sparql');

function playlist(req, res) {
	let data = {
		playlist: req.params.id,
		id: req.payload.id
	}

	if (!packets.verify.checkData(data)) {
		return res.sendInvalidRequest(res);
	}
	database.findOneInCollection("playlists", ["id", "name", "private", "owner_id", "modified"], {id: data.playlist })
	.then(function(playlist) {
		if (!playlist) {
			throw new errors.Request.Invalid("No matching playlist.");
		} else if (playlist.private && playlist.owner_id != data.id) {
			throw new errors.Request.Unauthorized("Playlist is private.");
		}
		database.getPlaylistSongs(data.playlist)
		.then(function(songs) {
			let promises = []
			songs.forEach(song => {
				promises.push(sparql.songs.fetch(song.uri));
			});
			Promise.all(promises)
			.then(function(fetchedSongs) {
				for (let i = 0; i < fetchedSongs.length; i++) {
					songs[i] = Object.assign({}, songs[i], fetchedSongs[i]);
				}
				res.status(200);
				return res.json(packets.factory.createResponse("Retrieved playlist.", packets.types.success, {playlist: playlist, songs: songs}));
			})
			.catch(function(err) {
				errors.intercept(res, err);
			});
		})
		.catch(function(err) {
			errors.intercept(res, err);
		});
	}).catch(function(err) {
		errors.intercept(res, err);
	});
}

function playlists(req, res) {
	if (req.payload == undefined || req.payload.id == undefined) {
		return res.sendInvalidRequest(res);
	} else if (Object.keys(req.query).length !== 0) {
		return search(req, res);
	}
	database.findAllPlaylists(req.payload.id)
	.then(function(playlists) {
		res.status(200);
		return res.json(packets.factory.createResponse("Retrieved playlists.", packets.types.success, {playlists: playlists}));
	})
	.catch(function(err) {
		errors.intercept(res, err);
	});
}

module.exports = {
	playlists,
	playlist
}