const database = require('../database');
const packets = require('../packets');
const errors = require('../errors');

function playlists(req, res) {
	let owner = req.query.owner;

	if (owner == undefined) {
		return res.sendInvalidRequest(res);
	}
	database.searchAllPlaylists(owner, false)
	.then(function(playlists) {
		res.status(200);
		return res.json(packets.factory.createResponse("Retrieved public playlists.", packets.types.success, {playlists: playlists}));
	}).catch(function(err) {
		errors.intercept(res, err);
	});
}

module.exports = {
	playlists
}