const database = require('../database');
const packets = require('../packets');
const errors = require('../errors');

function deletePlaylist(req, res) {
	let data = {
		playlist: req.params.id,
		id: req.payload.id
	}

	if (!packets.verify.checkData(data)) {
		return res.sendInvalidRequest(res);
	}
	database.deleteOneInCollection("playlists", {id: data.playlist, owner_id: data.id})
	.then(function(changes) {
		if (changes == 0) {
			throw new errors.Request.Invalid("No matching playlist found.");
		}
		res.status(200);
		res.json(packets.factory.createResponse("Playlist deleted.", packets.types.success, {}));
	}).catch(function(err) {
		errors.intercept(res, err);
	});
}

module.exports = {
	deletePlaylist
}