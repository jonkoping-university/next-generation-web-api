const express = require('express');
const router = express.Router();
const verify = require('../tokens/verify').verify;
const create = require('./create').createSong;
const get = require('./get').song;
const deleteSong = require('./delete').deleteSong;
const sparql = require('../sparql');

router.use(function(req, res, next) {
	verify(req, res, next);
});

router.get('/:id', function(req, res) {
	return get(req, res);
});
router.get('/', function(req, res) {
	return sparql.songs.search(req, res);
});
router.post('/', function(req, res) {
	return create(req, res);
});
router.delete('/:id', function(req, res) {
	return deleteSong(req, res);
});

module.exports = router;