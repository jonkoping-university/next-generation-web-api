const database = require('../database');
const packets = require('../packets');
const errors = require('../errors');
const sparql = require('../sparql');

function song(req, res) {
	let data = {
		song: req.params.id
	}

	if (!packets.verify.checkData(data)) {
		return res.sendInvalidRequest(res);
	}
	database.findOneInCollection("songs", ["id", "name", "uri"], {id: data.song})
	.then(function(song) {
		if (!song) {
			throw new errors.Request.Invalid("No matching song.");
		}
		return sparql.songs.fetch(song.uri);
	}).then(function(fetchedSong) {
		res.status(200);
		res.json(packets.factory.createResponse("Retrieved song.", packets.types.success, {song: Object.assign({}, song, fetchedSong)}));
	})
	.catch(function(err) {
		errors.intercept(res, err);
	});
}

module.exports = {
	song
}