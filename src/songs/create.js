const database = require('../database');
const packets = require('../packets');
const errors = require('../errors');

function createSong(req, res) {
	let data = {
		name: req.body.name,
		playlist: req.body.playlist,
		id: req.payload.id
	};

	if (!packets.verify.checkData(data)) {
		return res.sendInvalidRequest(res, "Missing informations.");
	}
	data.uri = req.body.uri ? req.body.uri.substring(0, req.body.uri.length - 1) : undefined;

	database.createSong(data.name, data.playlist, data.uri, data.id)
	.then(function(song) {
		if (song == 0) {
			throw new errors.Request.Invalid("Couldn't create song.");
		}
		database.updatePlaylist(data.playlist, data.id)
		.then(function(changes) {
			if (changes == 0) {
				throw new errors.Request.Invalid("No matching playlist found.");
			}
			res.status(200);
			res.json(packets.factory.createResponse("Song created.", packets.types.success, {id: song}));
		}).catch(function(err) {
			errors.intercept(res, err);
		});
	}).catch(function(err) {
		errors.intercept(res, err);
	});
}

module.exports = {
	createSong
}