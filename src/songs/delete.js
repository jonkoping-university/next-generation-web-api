const database = require('../database');
const packets = require('../packets');
const errors = require('../errors');

function deleteSong(req, res) {
	let data = {
		song: req.params.id,
		id: req.payload.id,
		playlist: Number(req.body.playlist)
	}

	if (!packets.verify.checkData(data)) {
		return res.sendInvalidRequest(res);
	}
	database.deleteOneInCollection("songs", {id: data.song, owner_id: data.id})
	.then(function(changes) {
		if (changes == 0) {
			throw new errors.Request.Invalid("No matching song found.");
		}
		return database.updatePlaylist(data.playlist, data.id);
	})
	.then(function(changes) {
		if (changes == 0) {
			throw new errors.Request.Invalid("No matching playlist found.");
		}
		res.status(200);
		res.json(packets.factory.createResponse("Song deleted.", packets.types.success, {}));
	}).catch(function(err) {
		errors.intercept(res, err);
	});
}

module.exports = {
	deleteSong
}