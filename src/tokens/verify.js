const fs = require('fs');
const publicKey = fs.readFileSync('public.key', 'utf8');
const jwt = require('jsonwebtoken');
const packets = require('../packets');

module.exports = {
	verify(req, res, next) {
		const data = {
			token: req.token,
		}
		const verifyOptions = {
			issuer: "PlaylistManager",
			expiresIn: "1h",
			algorithm: ["RS256"],
		};

		if (!packets.verify.checkData(data)) {
			return res.sendUnauthenticatedRequest(res, "Missing token.");
		}
		jwt.verify(data.token, publicKey, verifyOptions, function(err, decoded) {
			if (err) {
				return res.sendUnauthenticatedRequest(res, "Access denied.");
			}
			req.payload = decoded;
			next();
		});
	}
};