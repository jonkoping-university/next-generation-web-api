const fs = require('fs');
const privateKey = fs.readFileSync('private.key', 'utf8');
const jwt = require('jsonwebtoken');

module.exports = {
	generate(_, account) {
		const signOptions = {
			issuer: "PlaylistManager",
			expiresIn: "1h",
			algorithm: "RS256"
		};
		const payload = {
			id: account.id,
			email: account.email,
			fullname: account.fullname
		};
		return jwt.sign(payload, privateKey, signOptions);
	}
};