const axios = require('axios');
const errors = require('../errors');

class SPARQLClient {
	prefix = `
		PREFIX mo: <http://purl.org/ontology/mo/>
		PREFIX mbz: <http://purl.org/ontology/mbz#>
		PREFIX owl: <http://www.w3.org/2002/07/owl#>
		PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
		PREFIX bio: <http://purl.org/vocab/bio/0.1/>
		PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
		PREFIX tags: <http://www.holygoat.co.uk/owl/redwood/0.1/tags/>
		PREFIX geo: <http://www.geonames.org/ontology#>
		PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
		PREFIX lingvoj: <http://www.lingvoj.org/ontology#>
		PREFIX rel: <http://purl.org/vocab/relationship/>
		PREFIX vocab: <http://dbtune.org/musicbrainz/resource/vocab/>
		PREFIX event: <http://purl.org/NET/c4dm/event.owl#>
		PREFIX map: <file:/home/moustaki/work/motools/musicbrainz/d2r-server-0.4/mbz_mapping_raw.n3#>
		PREFIX db: <http://dbtune.org/musicbrainz/resource/>
		PREFIX foaf: <http://xmlns.com/foaf/0.1/>
		PREFIX dc: <http://purl.org/dc/elements/1.1/>
	`;

	constructor(endpointURL) {
		this.endpointURL = endpointURL + "?output=json&query="
	}

	fetchQuery(query) {
		query = this.endpointURL + encodeURIComponent(this.prefix + query);

		return axios.get(query);
	}

	getResource(resourceURI) {
		let query = `
			SELECT DISTINCT ?property ?hasValue ?isValueOf
			WHERE {
			{ <` + resourceURI + `> ?property ?hasValue }
			UNION
			{ ?isValueOf ?property <` + resourceURI + `> }
			}
			ORDER BY (!BOUND(?hasValue)) ?property ?hasValue ?isValueOf
		`;
		return new Promise((resolve, reject) => {
			this.fetchQuery(query)
			.then(function(response) {
				resolve(response.data && response.data.results && response.data.results.bindings ? response.data.results.bindings : []);
			})
			.catch(function(error) {
				console.error(error);
				reject(new errors.Server.Default("Couldn't find resource.", 500));
			})
		});
	}

	findProperty(properties, property) {
		let item = properties.find((element) => {
			return element.property.value === property;
		});
		if (item && (item.hasValue || item.isValueOf)) {
			return (item.hasValue || item.isValueOf).value;
		}
		return undefined;
	}

	createEntityFromProperties(properties, keys) {
		let entity = {};

		for (let key in keys) {
			entity[key] = this.findProperty(properties, keys[key]);
		};
		return entity;
	}
}

module.exports = SPARQLClient;