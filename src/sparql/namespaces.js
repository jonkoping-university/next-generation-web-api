const rdf = require('rdf');

module.exports = {
	foaf: rdf.ns('http://xmlns.com/foaf/0.1/'),
	mo: rdf.ns('http://purl.org/ontology/mo/'),
	mbz: rdf.ns('http://purl.org/ontology/mo/'),
	owl: rdf.ns('http://www.w3.org/2002/07/owl#'),
	xsd: rdf.ns('http://www.w3.org/2001/XMLSchema#'),
	bio: rdf.ns('http://purl.org/vocab/bio/0.1/'),
	rdfs: rdf.ns('http://www.w3.org/2000/01/rdf-schema#'),
	tags: rdf.ns('http://www.holygoat.co.uk/owl/redwood/0.1/tags/'),
	geo: rdf.ns('http://www.geonames.org/ontology#'),
	rdf: rdf.ns('http://www.w3.org/1999/02/22-rdf-syntax-ns#'),
	lingvoj: rdf.ns('http://www.lingvoj.org/ontology#'),
	rel: rdf.ns('http://purl.org/vocab/relationship/'),
	vocab: rdf.ns('http://dbtune.org/musicbrainz/resource/vocab/'),
	event: rdf.ns('http://purl.org/NET/c4dm/event.owl#'),
	map: rdf.ns('file:/home/moustaki/work/motools/musicbrainz/d2r-server-0.4/mbz_mapping_raw.n3#'),
	db: rdf.ns('http://dbtune.org/musicbrainz/resource/'),
	dc: rdf.ns('http://purl.org/dc/elements/1.1/')
};
