const client = new (require('./client'))('http://dbtune.org/musicbrainz/sparql');
const errors = require('../errors');
const packets = require('../packets');
const axios = require('axios');

function fetchAlbumCover(albumURI) {
	return new Promise((resolve) => {
		if (albumURI == undefined) {
			resolve("");
		}
		let url = albumURI.replace("http://dbtune.org/musicbrainz/resource/record/", "");
		url = "http://coverartarchive.org/release/" + url;
		axios.get(url)
		.then(function(response) {
			if (!response.data.images) {
				resolve(undefined);
			}
			let images = response.data.images;
			let image = images[0];
			let thumbnails = image.thumbnails;
			resolve(thumbnails ? thumbnails["500"] : undefined);
		})
		.catch(function(error) {
			resolve(undefined);
		});
	});
}

function createSongFromProperties(binding) {
	let song = {
		"name": binding.songTitle ? binding.songTitle.value : "",
		"link": binding.song ? binding.song.value : "",
		"musicbrainz": binding.songBrainz ? binding.songBrainz.value : "",
		"length": binding.songLength ? binding.songLength.value : "",
		"album": {
			"name": binding.albumTitle ? binding.albumTitle.value : "",
			"link": binding.album ? binding.album.value : "",
			"musicbrainz": binding.albumBrainz ? binding.albumBrainz.value : "",
		},
		"artist": {
			"name": binding.artistName ? binding.artistName.value : "",
			"link": binding.artist ? binding.artist.value : "",
			"musicbrainz": binding.artistBrainz ? binding.artistBrainz.value : "",
		}
	};
	let seconds = Number(song.length / 1000).toFixed(0) % 60;
	song["length"] = Math.floor(Number(song.length) / 1000 / 60) + ":" + (seconds < 10 ? "0" : "") + seconds;
	return song;
}

function fetchSong(songURI) {
	let query = `
		SELECT DISTINCT *
		WHERE {
			<` + songURI + `> a mo:Track ; dc:title ?songTitle ; foaf:maker ?artist ; mo:length ?songLength ; mo:musicbrainz ?songBrainz .
			?artist foaf:name ?artistName ; mo:musicbrainz ?artistBrainz .
			?album mo:track <` + songURI + `> ; dc:title ?albumTitle ; mo:musicbrainz ?albumBrainz
		}
	`;
	let song = {};

	return new Promise((resolve, reject) => {
		client.fetchQuery(query)
		.then(function(response) {
			if (response.data && response.data.results && response.data.results.bindings) {
				song = createSongFromProperties(response.data.results.bindings[0]);
				return fetchAlbumCover(song.album ? song.album.link : undefined);
			} else {
				resolve([]);
			}
		})
		.then(function(cover) {
			song.album["cover"] = cover;
			resolve(song);
		})
		.catch(function(error) {
			console.error(error);
			reject(new errors.Server.Default("Couldn't find songs.", 500));
		})
	});
}

function fetchSongs(songName) {
	let query = `
		SELECT DISTINCT *
		WHERE {
			?song a mo:Track ; dc:title "` + songName + `"; dc:title ?songTitle ; foaf:maker ?artist ; mo:length ?songLength ; mo:musicbrainz ?songBrainz .
			?artist foaf:name ?artistName ; mo:musicbrainz ?artistBrainz .
			?album mo:track ?song ; dc:title ?albumTitle ; mo:musicbrainz ?albumBrainz
		}
		LIMIT 100
	`;
	return new Promise((resolve, reject) => {
		client.fetchQuery(query)
		.then(function(response) {
			let coverPromises = []

			if (response.data && response.data.results && response.data.results.bindings) {
				let songs = []
				response.data.results.bindings.forEach(binding => {
					let song = createSongFromProperties(binding);
					coverPromises.push(fetchAlbumCover(song.album.link));
					songs.push(song);
				});
				Promise.all(coverPromises).then(function(covers) {
					for (let i = 0; i < covers.length; i++) {
						songs[i].album["cover"] = covers[i]
					}
					resolve(songs);
				});
			} else {
				resolve([]);
			}
		})
		.catch(function(error) {
			console.error(error);
			reject(new errors.Server.Default("Couldn't find songs.", 500));
		})
	});
}

function searchSongs(req, res) {
	if (!req.query.query) {
		res.status(200);
		return res.json(packets.factory.createResponse("Songs fetched.", packets.types.success, {songs: []}));
	}
	fetchSongs(req.query.query)
	.then(function(songs) {
		res.status(200);
		res.json(packets.factory.createResponse("Songs fetched.", packets.types.success, {songs: songs}));
	})
	.catch(function(err) {
		errors.intercept(res, err);
	})
}

module.exports = {
	search: searchSongs,
	fetch: fetchSong
}