#!/usr/bin/env python3.7

import sys, pandas, math, urllib
from rdflib.namespace import FOAF
from rdflib import URIRef, Graph, XSD, Literal, RDF, Namespace

def loadData(file_path):
	try:
		data = pandas.read_csv(file_path, sep=',', encoding = "ISO-8859-1", keep_default_na=False)
	except Exception as error:
		print(error)
		print("Couldn't open dataset.", file=sys.stderr)
		exit(84)
	return data

def bindOntologies(store):
	ontologies = {
		"dbo": Namespace("http://dbpedia.org/ontology/"),
		"dbp": Namespace("http://dbpedia.org/property/"),
		"dbr": Namespace("http://dbpedia.org/resource/"),
		"ocds": Namespace("http://purl.org/onto-ocds/ocds"),
		"ov": Namespace("http://open.vocab.org/"),
		"rdfs": Namespace("http://www.w3.org/2000/01/rdf-schema#"),
		"xsd": Namespace("http://www.w3.org/2001/XMLSchema#"),
		"vcard": Namespace("http://www.w3.org/2006/vcard/"),
		"mo": Namespace("http://purl.org/ontology/mo/"),
		"dc": Namespace("http://purl.org/dc/elements/1.1/"),
		"tl": Namespace("http://purl.org/NET/c4dm/timeline.owl#"),
		"event": Namespace("http://purl.org/NET/c4dm/event.owl#"),
		"foaf": FOAF
	}
	for key in ontologies.keys():
		store.bind(key, ontologies[key], override=False)
	return ontologies

def createProperty(entry, row, subject):
	propertyName = entry["name"]
	if row[propertyName] == None or row[propertyName] == "":
		return None
	value = row[propertyName]
	if entry["type"] == XSD.anyURI:
		value = value.replace(' ', '_')
	if "prefix" in entry.keys():
		value = entry["prefix"] + str(value)
	if "suffix" in entry.keys():
		value = value + entry["suffix"]
	if entry["type"] == Literal:
		value = entry["type"](value, datatype=entry["datatype"])
	else:
		value = entry["type"](value)
	return (subject, entry["rep"], value)

def createEntity(store, ontologies, ref, type, properties, data):
	entity = URIRef(ref)
	store.add((entity, RDF.type, type))
	for entry in properties:
		tmp = createProperty(entry, data, entity)
		if tmp:
			store.add(tmp)
	return True

def createGraph(data):
	store = Graph()
	ontologies = bindOntologies(store)
	songProperties = [
		{"name": "Track.Name", "rep": ontologies["dc"].title, "type": Literal, "datatype": XSD.string},
		{"name": "Maker", "rep": ontologies["foaf"].maker, "type": URIRef},
		{"name": "Genre", "rep": ontologies["mo"].genre, "type": URIRef},
		{"name": "Length.", "rep": ontologies["mo"].duration, "type": Literal, "datatype": XSD.float}
	]
	artistProperties = [
		{"name": "Artist.Name", "rep": ontologies["foaf"].name, "type": Literal, "datatype": XSD.string},
	]
	genreProperties = [
		{"name": "Genre", "rep": ontologies["dc"].title, "type": Literal, "datatype": XSD.string},
	]
	for _, row in data.iterrows():
		createEntity(store, ontologies, urllib.parse.quote_plus( "artist-" + str(row['Artist.Name'])), ontologies["mo"].MusicArtist, artistProperties, row)
		row['Maker'] = urllib.parse.quote_plus("artist-" + str(row['Artist.Name']))
		createEntity(store, ontologies, urllib.parse.quote_plus( "genre-" + str(row['Genre'])), ontologies["mo"].Genre, genreProperties, row)
		row['Genre'] = urllib.parse.quote_plus("genre-" + str(row['Genre']))
		createEntity(store, ontologies, "track-" + str(row['ID']), ontologies["mo"].Track, songProperties, row)
	return store

def saveGraph(store):
	save = open('Music.ttl', 'wb')
	save.write(store.serialize(format="turtle"))
	save.close()

def main():
	if len(sys.argv) != 2:
		print("Please provide path to dataset.", file=sys.stderr)
		exit(84)
	data = loadData(sys.argv[1])
	store = createGraph(data)
	saveGraph(store)

main()