const path = require('path');
const appDir = path.dirname(require.main.filename);
const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database(appDir + '/data/database');
const errors = require('../errors');

function buildSearchFields(fields) {
	let result = "";

	if (fields.length == 0) {
		result += "*";
	} else {
		for (let i = 0; i < fields.length; i++) {
			result += fields[i];
			if (i + 1 != fields.length) {
				result += ", ";
			}
		}
	}
	return result;
}

function buildQueryParams(params, parsedParams) {
	let result = "";

	if (params != {}) {
		result += " WHERE ";
		const keys = Object.keys(params);

		for (key of keys) {
			result += key + " = ?";
			parsedParams.push(params[key]);
			if (keys.indexOf(key) + 1 != keys.length) {
				result += " AND ";
			}
		}
	}
	return result;
}

function processQueryResult(resolve, reject, err, result) {
	if (err) {
		reject(new errors.Server.Database(err.message));
	} else {
		resolve(result);
	}
}

class Database {
	constructor() {
		this.db = undefined;
		this.connectDatabase();
	}

	connectDatabase() {
		if (this.db === undefined) {
			this.db = new sqlite3.Database(appDir + '/data/database', (err) => {
				if (err) {
					console.error('Could not connect to database', err)
				} else {
					console.error('Connected to database');
					this.createTables();
				}
			});
		}
	}

	createTables() {
		db.run("CREATE TABLE IF NOT EXISTS accounts (id INTEGER PRIMARY KEY AUTOINCREMENT, fullname TEXT(120) NOT NULL DEFAULT '', `email` TEXT(320) UNIQUE NOT NULL DEFAULT '', `password` TEXT(320) NOT NULL DEFAULT '')");
		db.run("CREATE TABLE IF NOT EXISTS playlists (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT(120) NOT NULL DEFAULT '', private NUMERIC BOOLEAN NOT NULL DEFAULT FALSE, `owner_id` INTEGER NOT NULL, modified NUMERIC DATETIME NOT NULL DEFAULT 1580289775, FOREIGN KEY(owner_id) REFERENCES accounts(id) ON DELETE CASCADE)");
		db.run("CREATE TABLE IF NOT EXISTS songs (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT(320) NOT NULL DEFAULT '', uri TEXT(1024) DEFAULT NULL, `playlist_id` INTEGER NOT NULL, `owner_id` INTEGER NOT NULL, FOREIGN KEY(playlist_id) REFERENCES playlists(id) ON DELETE CASCADE, FOREIGN KEY(owner_id) REFERENCES accounts(id) ON DELETE CASCADE)");
	}

	run(query, params = []) {
		if (this.db === undefined) {
			throw new errors.Server.Default("Database doesn't exist.");
		}
		return new Promise((resolve, reject) => {
			db.run(query, params, function(err) {
				let result = {object: this};
				processQueryResult(resolve, reject, err, result);
			});
		});
	}

	get(query, params = []) {
		if (this.db === undefined) {
			throw new errors.Server.Default("Database doesn't exist.");
		}
		return new Promise((resolve, reject) => {
			db.get(query, params, (err, result) => {
				processQueryResult(resolve, reject, err, result);
			});
		});
	}

	all(query, params = []) {
		if (this.db === undefined) {
			throw new errors.Server.Default("Database doesn't exist.");
		}
		return new Promise((resolve, reject) => {
			db.all(query, params, (err, result) => {
				processQueryResult(resolve, reject, err, result);
			});
		});
	}

	createAccount(fullname, email, password) {
		return this.run("INSERT INTO accounts(fullname, email, password) VALUES(?, ?, ?)", [fullname, email, password]);
	}

	searchAllAccounts(req) {
		return this.all("SELECT id, fullname FROM accounts WHERE id != ? AND fullname LIKE ? ORDER BY fullname ASC", [req.payload.id, "%" + req.query.query + "%"]);
	}

	findAllAccounts(req) {
		return this.all("SELECT id, fullname FROM accounts WHERE id != ? ORDER BY fullname ASC", [req.payload.id]);
	}

	getPlaylistSongs(playlist) {
		return this.all("SELECT songs.id, songs.name, songs.uri, songs.playlist_id FROM songs LEFT JOIN playlists ON playlists.id = songs.playlist_id WHERE playlists.id = ?", [playlist]);
	}

	findAllPlaylists(account) {
		return this.all("SELECT id, name, private, owner_id, modified FROM playlists WHERE owner_id = ? ORDER BY modified DESC", [account]);
	}

	searchAllPlaylists(account) {
		return this.all("SELECT id, name, private, owner_id, modified FROM playlists WHERE owner_id = ? AND private = ? ORDER BY modified DESC", [account, false]);
	}

	createPlaylist(name, owner, isPrivate, modified) {
		return new Promise((resolve, reject) => {
			this.run("INSERT INTO playlists(name, owner_id, private, modified) VALUES(?, ?, ?, ?)", [name, owner, isPrivate, modified])
			.then(function(result) {
				resolve(result.object.lastID);
			}).catch(function(err) {
				reject(err);
			});
		});
	}

	createSong(name, playlist, uri, owner) {
		return new Promise((resolve, reject) => {
			this.run("INSERT INTO songs(owner_id, playlist_id, name, uri) SELECT playlists.owner_id, id as playlist_id, ?, ? FROM playlists WHERE id = ? AND owner_id = ?", [name, uri, playlist, owner])
			.then(function(result) {
				resolve(result.object.lastID);
			}).catch(function(err) {
				reject(err);
			});
		});
	}

	findOneInCollection(table, fields = [], params = {}) {
		let query = "SELECT ";
		let parsedParams = [];

		query += buildSearchFields(fields);
		query += " FROM " + table;
		query += buildQueryParams(params, parsedParams);
		return this.get(query, parsedParams);
	}

	deleteOneInCollection(table, params = {}) {
		let query = "DELETE FROM " + table;
		let parsedParams = [];

		query += buildQueryParams(params, parsedParams);
		return new Promise((resolve, reject) => {
			this.run(query, parsedParams)
			.then(function(result) {
				resolve(result.object.changes);
			}).catch(function(err) {
				reject(err);
			});
		});
	}

	updatePlaylist(playlist, owner, name, isPrivate) {
		let query = "UPDATE playlists SET name = ?, private = ?, modified = ? WHERE id = ? AND owner_id = ?";
		let params = [name, isPrivate, Date.now() / 1000, playlist, owner];

		if (name == undefined) {
			query = "UPDATE playlists SET modified = ? WHERE id = ? AND owner_id = ?";
			params = [Date.now() / 1000, playlist, owner];
		}
		return new Promise((resolve, reject) => {
			this.run(query, params)
			.then(function(result) {
				resolve(result.object.changes);
			}).catch(function(err) {
				reject(err);
			});
		});
	}
}

module.exports = new Database();